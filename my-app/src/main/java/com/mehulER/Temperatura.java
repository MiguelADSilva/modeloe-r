package com.mehulER;

import java.util.Date;

public class Temperatura {
    private int grausC;
    private Date date;

    public Temperatura(int grausC) {
        this.grausC = grausC;
        this.date = new Date();
    }

    public int getGrausC() {
        return grausC;
    }

    public Date getDate() {
        return date;
    }

    public void setGrausC(int grausC) {
        this.grausC = grausC;
    }
}
