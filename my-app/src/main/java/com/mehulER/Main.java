package com.mehulER;

public class Main {
    public static void main( String[] args ) {

        Country pt = new Country("Portugal");

        City city1 = new City("Porto","41°08′58″ N","8°36′39″ O",pt);
        City city2 = new City("Lisboa","41°08′58″ N","8°36′39″ O",pt);
        City city3 = new City("Braga","41°08′58″ N","8°36′39″ O",pt);

        Temperatura temp1 = new Temperatura(22);
        Temperatura temp2 = new Temperatura(24);
        Temperatura temp3 = new Temperatura(25);
        Temperatura temp4 = new Temperatura(30);

        city1.addTemperatura(temp1);
        city1.addTemperatura(temp2);
        city1.addTemperatura(temp3);

        city2.addTemperatura(temp4);
        city2.addTemperatura(temp3);

        city3.addTemperatura(temp4);
        city3.addTemperatura(temp3);
        city3.addTemperatura(temp2);

        System.out.println("Temperatura mais alta do "+city1.getNameCity()+" é "+city1.getHighTempXDays(10));
        System.out.println("------------------------>");
        System.out.println("Temperatura mais alta do "+city2.getNameCity()+" é "+city2.getHighTempXDays(10));
        System.out.println("------------------------>");
        System.out.println("Temperatura mais alta do "+city3.getNameCity()+" é "+city3.getHighTempXDays(10));
        System.out.println("//--------------------------");
        pt.printCities();
        System.out.println("//--------------------------");
        System.out.println(city1.getHightCertainCities("2019-02-14"));
        System.out.println("------------------------>");
        System.out.println(city2.getHightCertainCities("2019-02-14"));
        System.out.println("------------------------>");
        System.out.println(city3.getHightCertainCities("2019-02-14"));

    }
}
