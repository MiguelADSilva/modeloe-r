package com.mehulER;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class City {
    private LinkedList<Temperatura> value;
    private String NameCity;
    private String Latitude;
    private String Longitude;
    private Country Country;
    private static final ThreadLocal<DateFormat> dateFormatThreadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyyMMdd"));

    public City(String nameCity, String latitude, String longitude, Country country) {
        this.value = new LinkedList<>();
        this.NameCity = nameCity;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.Country = country;
        this.Country.addCities(this);
    }

    public void addTemperatura(Temperatura temp) {
        this.value.add(temp);
    }

    public LinkedList<Temperatura> getValue() {
        return value;
    }

    public String getNameCity() {
        return NameCity;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public Country getCountry() {
        return Country;
    }

    public void setValue(LinkedList<Temperatura> value) {
        this.value = value;
    }

    public void setNameCity(String nameCity) {
        NameCity = nameCity;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public void setCountry(Country country) {
        Country = country;
    }

    public int getHighTempXDays(int days) {
        LinkedList<Integer> tempList = new LinkedList<>();
        int highest = 0;

        Instant now = Instant.now();
        Instant nowMinusDays = now.minus(Duration.ofDays(days));
        Date dateNow = Date.from(now);
        Date dateNowMinusDays = Date.from(nowMinusDays);
        for (Temperatura temp : this.value) {
            if (dateNowMinusDays.compareTo(temp.getDate()) == -1) {
                tempList.add(temp.getGrausC());
            }
        }

        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i) > highest) {
                highest = tempList.get(i);
            }
        }
        return highest;
    }


    public int getHightCertainCities(String day) {
        int highest = 0;
        try {
            Date date = convertToDate(day);
            for(Temperatura value : this.value){
                if(compareDates(date, value.getDate()) == 0){
                    if(value.getGrausC() > highest){
                        highest = value.getGrausC();
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return highest;
    }

    public static int compareDates(Date date1, Date date2) {
        DateFormat dateFormat = dateFormatThreadLocal.get();
        return dateFormat.format(date1).compareTo(dateFormat.format(date2));
    }

    public static Date convertToDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(date);
    }
}
