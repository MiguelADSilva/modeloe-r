package com.mehulER;

import java.util.LinkedList;

public class Country {
    private LinkedList<City> cities;
    private String countryName;

    public Country(String countryName) {
        this.cities = new LinkedList<>();
        this.countryName = countryName;
    }

    public LinkedList<City> getCities() {
            return cities;
    }

    public void addCities(City cityName){
        cities.add(cityName);
    }

    public void printCities(){
        for(City city : cities){
            System.out.println(city.getNameCity());
        }
    }

}
